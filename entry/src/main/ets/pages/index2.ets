/**
 * BSD License
 *
 * Copyright (c) 2023 Huawei Device Co., Ltd. All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 *
 * * Redistributions of source code must retain the above copyright notice, this
 *  list of conditions and the following disclaimer.
 *
 * * Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 *
 * * Neither the name Facebook nor the names of its contributors may be used to
 * endorse or promote products derived from this software without specific
 * prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 'AS IS' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
 * ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

import rebound from "@ohos/rebound";

let springSystem: rebound.SpringSystem = new rebound.SpringSystem();
let spring: rebound.Spring = springSystem.createSpring();
let springConfig: rebound.SpringConfig;

@Entry
@Component
struct Index2 {
  @State public frictionSpringVal: number = 0
  @State public imageSize: number = 1
  @State public imgPath: string = "arrow_up.png";
  @State public showComponent: Visibility = Visibility.Hidden
  @State public origamiFriction: number = 10;

  build() {
    Stack({ alignContent: Alignment.Bottom }) {
      Image($rawfile('landscape.jpg'))
        .width('100%')
        .height('100%')
        .scale({ x: this.imageSize, y: this.imageSize })
        .onTouch((event: TouchEvent) => {
          if (event.type == TouchType.Down) {
            this.initSpring();
            spring.setEndValue(1);
          } else if (event.type == TouchType.Up || event.type == TouchType.Cancel) {
            spring.setEndValue(0);
          }
        })
      Row() {
        Column() {
          Text('OrigamiFriction')
            .fontSize(20)
            .fontWeight(FontWeight.Bold)
            .margin({ top: 15 })
            .alignSelf(ItemAlign.Start)
          Text(this.frictionSpringVal.toFixed(2).toString())
            .fontSize(20)
            .fontWeight(FontWeight.Regular)
        }

        Column() {
          Slider({ "value": this.origamiFriction, min: 1, max: 30, step: 1 })
            .showTips(true)
            .onChange((value, mode) => {
              this.origamiFriction = value;
              this.setFriction(value);
            })
            .blockColor(Color.Blue)
            .margin({ top: 10 })
        }
        .alignSelf(ItemAlign.Center)
        .padding({ top: 10 })
      }
      .margin({ bottom: 60 })
      .alignSelf(ItemAlign.End)
      .width('100%')
      .height('12%')
      .backgroundColor(Color.White)
      .visibility(this.showComponent)

      Image($rawfile(this.imgPath))
        .height("8%")
        .width("15%")
        .alignSelf(ItemAlign.End)
        .onTouch((event: TouchEvent) => {
          if (event.type == TouchType.Down) {
            if (this.showComponent == Visibility.Visible) {
              this.showComponent = Visibility.Hidden;
              this.imgPath = "arrow_up.png"
            } else {
              this.showComponent = Visibility.Visible;
              this.imgPath = "arrow_down.png"
            }
          }
        })
    }
  }

  initSpring() {
    spring.setVelocity(10);
    spring.setSpringConfig(rebound.SpringConfig.coastingConfigWithOrigamiFriction(this.origamiFriction));
    springConfig = spring.getSpringConfig();
    this.frictionSpringVal = springConfig.friction;
    spring.addListener({
      onSpringUpdate: (spring: rebound.Spring) => {
        let val: number = spring.getCurrentValue();
        val = rebound.MathUtil.mapValueInRange(val, 0, 1, 1, 0.5);
        this.imageSize = val;
      },
      onSpringEndStateChange: () => {
      }
    });
  }

  setFriction(frictionval: number) {
    spring.setSpringConfig(rebound.SpringConfig.coastingConfigWithOrigamiFriction(frictionval));
    this.frictionSpringVal = spring.getSpringConfig().friction;
    this.imageSize = 1.0;
  }
}
