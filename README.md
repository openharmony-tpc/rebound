# rebound

# Introduction

Rebound is a library that simulates spring dynamics and is used to drive physical animations.

## How to Install

```javascript
ohpm install @ohos/rebound
```

For details about the OpenHarmony ohpm environment configuration, see [OpenHarmony HAR](https://gitcode.com/openharmony-tpc/docs/blob/master/OpenHarmony_har_usage.en.md).

# How to Use
```
import rebound from '@ohos/rebound';
```

Feature 1: Create a spring system to maintain spring objects. You can create and register a spring object by configuring its tension, friction, and animation values, and add a listener to receive the spring position.

```javascript
spring: rebound.Spring = springSystem.createSpring();
spring.setSpringConfig(rebound.SpringConfig.DEFAULT_ORIGAMI_SPRING_CONFIG);
springConfig: rebound.SpringConfig = spring.getSpringConfig();
spring.addListener({
  onSpringUpdate: function(spring) {
    var val = spring.getCurrentValue();
    // Use Spring val to Process/Animate
  },
  onSpringEndStateChange: function(spring){
  }
});
```

Feature 2: Set a spring animation to run between 0 and *n*, which is the end value set by calling **Spring.setEndValue()**. In the code snippet below, the spring position changes between 0 and 1.

```javascript
spring.setEndValue(1);
```

Feature 3: Create an animation effect for the component by dynamically changing its position, which is achieved through the constantly changing spring. Call **mapValueInRange()** to convert the spring position to a user-defined range. In the code snippet below, the spring position is converted to an image size scale ratio to implement an animation effect.

```javascript
spring.addListener({
  onSpringUpdate: function(spring) {
    var val = spring.getCurrentValue();
    val = rebound.MathUtil
    .mapValueInRange(val, 0, 1, 1, 0.5);
    self.imageSize=val;
  },
  onSpringEndStateChange: function(spring){
  }
});
```

Feature 4: Call **spring.setEndValue(0)** to end the spring animation effect.

```javascript
spring.setEndValue(0);
```

### Available APIs

#### SpringSystem APIs

| API                              | Parameter                               | Return Value  | Description                                                        |
| ------------------------------------ | ----------------------------------- | ------------- | ------------------------------------------------------------ |
| SpringSystem()                       | Looper                              | StringSystem  | A spring system is iterated by a looper, which is responsible for executing each frame when the spring system is in the idle state. The spring system has three types of loopers: animation looper, simulation looper, and stepping animation looper. The animation looper is the default one because it is often used for UI animations.|
| createSpring()                       | tension?: number, friction?: number | Spring        | Creates a spring, which will be executed in a physical iteration loop. By default, the spring will use the default Origami spring configuration, with 40 tension and 7 friction. Custom values are also allowed.|
| createSpringWithBouncinessAndSpeed() | bounciness?: number, speed?: number | Spring        | Creates a spring with the specified bounciness and speed. To replicate Origami compositions based on PopAnimation patches, you can use this factory method to create a matching spring.|
| createSpringWithConfig():            | kwargs: SpringConfig                | Spring        | Creates a spring with the specified configuration.                        |
| getIsIdle()                          | None                                | boolean       | Checks whether the spring system is idle or active. This API returns **true** if all springs in the spring system are at rest, meaning the physical forces acting upon them have reached equilibrium.|
| getSpringById()                      | id: number                          | Spring        | Obtains a spring from the spring system by ID. It can be used to check the state of the spring before or after an integration loop in the spring system is executed.|
| getAllSprings()                      | None                                | Array\<Spring> | Obtains all springs registered with the spring system.                      |
| registerSpring()                     | value: Spring                       | void          | Registers a spring with the spring system. This API is automatically called once a spring is created by using **createSpring**. The spring can be executed only after the registration.|
| deregisterSpring(value: Spring)      | value: Spring                       | void          | Deregisters a spring from the spring system. Once this API is called for a spring, the spring system will not execute the spring during its integration loop. Normally, this API is automatically called when **destroy** is called.|
| loop()                               | currentTimeMillis: number           | void          | Moves the simulation forward through time. Each time a looper is executed, **onBeforeIntegrate** is called on all listeners that have been registered with the spring system. Any constraints or adjustments that should be enforced on the spring can be made before each iteration loop.|
| activateSpring()                     | springId: number                    | void          | Notifies the spring system that a spring has been displaced. The system starts the looper once it is idle.|


#### Spring APIs

| API                          | Parameter                                         | Return Value      | Description                                                        |
| -------------------------------- | --------------------------------------------- | ------------ | ------------------------------------------------------------ |
| Spring()                         | springSystem                                  | String       | Provides a classical spring model to emulate the transition of an object from a dynamic state to a state of rest.    |
| destroy()                        | None                                          | void         | Destroys a spring object.                                          |
| getId()                          | None                                          | number       | Obtains the spring ID.                                                  |
| getSpringConfig()                | None                                          | SpringConfig | Obtains the configuration of this spring.                                        |
| getCurrentValue()                | None                                          | number       | Obtains the current position of this spring.                                        |
| setCurrentValue()                | currentValue: number, skipSetAtRest?: boolean | Spring       | Sets the current position for this spring.                                        |
| getStartValue()                  | None                                          | number       | Obtains the start position of this spring. It is used to determine the number of oscillations that have occurred.        |
| getCurrentDisplacementDistance() | None                                          | number       | Obtains the distance of the spring from its resting position (end value).                              |
| getEndValue()                    | None                                          | number       | Obtains the resting position of this spring.                                  |
| setEndValue()                    | value: number                                 | Spring       | Sets the resting position for this spring. If the value is different from the current value, the spring system is notified and initiates its iterative process to enable the spring to reach an equilibrium state. All listeners that have registered the event **onSpringEndStateChange** will also be notified of this update immediately.|
| getVelocity()                    | None                                          | number       | Obtains the current velocity of this spring, in pixels per second.                         |
| setVelocity()                    | value: number                                 | Spring       | Sets the current velocity for this spring, in pixels per second. This is useful when you are performing a direct manipulation gesture. When a UI element is released, you can call **setVelocity** on the animation spring so that the spring continues with the same velocity as when the gesture ended with. The friction, tension, and displacement of the spring control its motion until the spring gradually comes to rest on a natural feeling curve.|
| getRestSpeedThreshold()          | None                                          | number       | Obtains the threshold of the movement velocity of this spring.                                  |
| setRestSpeedThreshold()          | value: number                                 | Spring       | Sets a threshold for the movement velocity of this spring. The spring is considered to be rest when its movement velocity falls below this threshold.|
| getRestDisplacementThreshold()   | None                                          | number       | Obtains the threshold of the rest displacement of this spring.                                  |
| isOvershootClampingEnabled()     | None                                          | boolean      | Checks whether overshoot clamping is enabled for this spring.                                |
| setOvershootClampingEnabled()    | value: boolean                                | Spring       | Enables overshoot clamping. When the spring reaches its resting position, it stops immediately, regardless of the current momentum. This is useful for certain types of animations that should not oscillate such as scaling down to 0 or alpha fade.|
| isOvershooting()                 | None                                          | boolean      | Checks whether the spring has gone past the end point by comparing its movement direction at the beginning with the current position and the end value.|
| isAtRest()                       | None                                          | boolean      | Checks whether the spring is at rest, which means that its current value is the same as the end value and it has no velocity. The thresholds for rest velocity and displacement described earlier define the bounds of this equivalence check. If the tension of the spring is 0, the spring is considered at rest when its velocity is lower than **restSpeedThreshold**.|
| setAtRest()                      | None                                          | Spring       | Sets the spring to be at rest.                                      |
| addListener()                    | value: Listener                               | Spring       | Adds a listener to the spring system to receive pre- or post-integration notifications that allow the spring to be constrained or adjusted.|
| removeListener()                 | value: Listener                               | Spring       | Removes a listener that has been added to the spring system.                      |
| removeAllListeners()             | None                                          | Spring       | Removes all listeners that have been added to the spring system.                    |
|                                  |                                               |              |                                                              |

#### Listener APIs

| API                  | Parameter          | Return Value| Description                                                        |
| ------------------------ | -------------- | ------ | ------------------------------------------------------------ |
| onSpringEndStateChange() | spring: Spring | void   | Called when the spring reaches the end state.                                            |
| onBeforeIntegrate()      | spring: Spring | void   | Called on any listener that has been registered with the spring system. This gives you the opportunity to apply any constraints or adjustments that should be enforced to the springs before each iteration loop.|
| onAfterIntegrate()       | spring: Spring | void   | Called on any listener that has been registered with the spring system. This gives you an opportunity to run any post integration constraints or adjustments on the springs in the    spring system.|
| onSpringActivate()       | spring: Spring | void   | Called when the spring is activated.                                              |
| onSpringUpdate           | spring: Spring | void   | Callback when the spring state is updated.                                          |
| onSpringAtRest()         | spring: Spring | void   | Called when the spring stops running.                                          |

#### SpringConfig APIs

| API                           | Parameter                             | Return Value      | Description                                                        |
| --------------------------------- | --------------------------------- | ------------ | ------------------------------------------------------------ |
| SpringConfig()                    | tension: number, friction: number | SpringConfig | Creates a spring configuration without tension or a coasting spring with a certain friction so that it does not coast infinitely.|
| fromOrigamiTensionAndFriction()   | tension: number, friction: number | SpringConfig | Converts an Origami spring tension and friction to Rebound spring constants. If you are using Origami to prototyping a design, this makes it easy to make your springs behave exactly the same in Rebound.|
| fromBouncinessAndSpeed            | bounciness: number, speed: number | SpringConfig | Converts an Origami PopAnimation spring bounciness and speed to Rebound spring constants. If you are using PopAnimation patches in Origami, this utility provides springs that match your prototype.|
| coastingConfigWithOrigamiFriction | friction: number                  | SpringConfig | Creates a spring configuration without tension or a coasting spring with a certain friction so that it does not coast infinitely. When it is used with **setVelocity**, the spring can move in a spring dynamic manner.|

#### Looper APIs

| API                    | Parameter| Return Value                  | Description                                                        |
| -------------------------- | ---- | ------------------------ | ------------------------------------------------------------ |
| AnimationLooper()          | None | AnimationLooper          | Plays each frame of the spring system in an animation timing loop. Since the animation looper is the most common when developing the UI, it is the default type of looper for a new spring system.|
| SimulationLooper()         | None | SimulationLooper         | Synchronously generates a pre-recorded animation that can be played in a timing loop later.          |
| SteppingSimulationLooper() |      | SteppingSimulationLooper | Resolves the spring system one step at a time controlled by an external loop.                          |

#### OrigamiValueConverter APIs

| API                    | Parameter            | Return Value| Description                          |
| -------------------------- | ---------------- | ------ | ------------------------------ |
| tensionFromOrigamiValue()  | oValue: number   | number | Converts an Origami value to tension.|
| origamiValueFromTension()  | tension: number  | number | Converts tension to an Origami value.|
| frictionFromOrigamiValue() | oValue: number   | number | Converts an Origami value to friction.|
| origamiFromFriction()      | friction: number | number | Converts friction to an Origami value.|

#### MathUtil APIs

| API           | Parameter                                                        | Return Value| Description                          |
| ----------------- | ------------------------------------------------------------ | ------ | ------------------------------ |
| mapValueInRange() | value: number, fromLow: number, fromHigh: number, toLow: number, toHigh: number | number | Maps a value from one range to another.|
| interpolateColor  | val: number, startColorStr: string, endColorStr: string      | string | Interpolates two hexadecimal colors.          |
| degreesToRadians  | deg: number                                                  | number | Converts degrees to Randian values.          |
| radiansToDegrees  | rad: number                                                  | number | Converts Randian values to degrees.           |

#### util APIs

| API       | Parameter            | Return Value| Description                                                        |
| ------------- | ---------------- | ------ | ------------------------------------------------------------ |
| bind()        | func, context    | void   | Binds a function to a context object.                                      |
| extend()      | target, source   | void   | Adds all the properties in the source to the target.                              |
| removeFirst() | array, item      | void   | Removes the first occurrence of the reference in the array.                                |
| hexToRGB()    | colorString      | string | Converts a color from the hexadecimal format to the RGB format. It is very convenient to perform color tweening animations.|
| rgbToHex()    | rNum, gNum, bNum | string | Converts a color from the RGB format to the hexadecimal format.                                               |

#### BouncyConversion APIs

| API           | Parameter                             | Return Value          | Description                                                        |
| ------------------ | --------------------------------- | ---------------- | ------------------------------------------------------------ |
| BouncyConversion() | bounciness: number, speed: number | BouncyConversion | Provides mathematical methods for converting from Origami PopAnimation configuration values to regular Origami tension and friction values.|

## About obfuscation
- Code obfuscation, please see[Code Obfuscation](https://docs.openharmony.cn/pages/v5.0/zh-cn/application-dev/arkts-utils/source-obfuscation.md)
- If you want the rebound library not to be obfuscated during code obfuscation, you need to add corresponding exclusion rules in the obfuscation rule configuration file obfuscation-rules.txt：
```
-keep
./oh_modules/@ohos/rebound
```

## Constraints
This project has been verified in the following versions:

DevEco Studio: (5.0.3.122), SDK: API 12 (5.0.0.17)

DevEco Studio: 4.1 Canary (4.1.3.317), OpenHarmony SDK: API 11 (4.1.0.36)

## Directory Structure

```
|---- rebound  
|     |---- entry  # Sample code
|     |---- rebound  # Rebound library folder
|           |---- rebound.js  # External APIs of rebound
|     |---- README.MD  # Readme      
|     |---- README_zh.MD  # Readme                 
```

## How to Contribute

If you find any problem when using rebound, submit an [issue](https://gitcode.com/openharmony-tpc/rebound/issues) or [PR](https://gitcode.com/openharmony-tpc/rebound/pulls).

## License

This project is licensed under [BSD License](https://gitcode.com/openharmony-tpc/rebound/blob/master/LICENSE).
