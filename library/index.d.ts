/**
 * BSD License
 *
 * Copyright (c) 2023 Huawei Device Co., Ltd. All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 *
 * * Redistributions of source code must retain the above copyright notice, this
 *  list of conditions and the following disclaimer.
 *
 * * Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 *
 * * Neither the name Facebook nor the names of its contributors may be used to
 * endorse or promote products derived from this software without specific
 * prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 'AS IS' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
 * ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

export interface Looper {
  springSystem: SpringSystem;

  run(): void;
}

export interface CompleteListener {
  onSpringEndStateChange: (spring?: Spring) => void;
  onBeforeIntegrate: (spring?: Spring) => void;
  onAfterIntegrate: (spring?: Spring) => void;
  onSpringActivate: (spring?: Spring) => void;
  onSpringUpdate: (spring?: Spring) => void;
  onSpringAtRest: (spring?: Spring) => void;
}

export type Listener = Partial<CompleteListener>;

export type NewPosition = {
  tempPosition: number,
  tempVelocity: number,
  position: number,
  velocity: number
}

export type PhysicsState = {
  position: number,
  friction: number,
};

export type RGB = {
  r: number,
  g: number,
  b: number,
};

export class SpringSystem {
  looper: Looper;
  listeners: Array<Listener> | null;
  _idleSpringIndices: Array<number>

  constructor(looper?: Looper);

  setLooper(looper: Looper): void;

  createSpring(tension?: number, friction?: number): Spring;

  createSpringWithBouncinessAndSpeed(bounciness?: number, speed?: number): Spring;

  createSpringWithConfig(springConfig: SpringConfig): Spring;

  getIsIdle(): boolean;

  getSpringById(id: string): Spring;

  getAllSprings(): Array<Spring>;

  registerSpring(spring: Spring): void;

  deregisterSpring(value: Spring): void;

  advance(time: number, deltaTime: number): void;

  loop(currentTimeMillis: number): void;

  activateSpring(springId: string): void;

  addListener(listener: Listener): void;

  removeListener(listener: Listener): void;

  removeAllListeners(): void;
}

export class Spring {
  static _ID: number;
  static MAX_DELTA_TIME_SEC: number;
  static SOLVER_TIMESTEP_SEC: number;

  constructor(system: SpringSystem);

  destroy(): void;

  getId(): string;

  setSpringConfig(value: SpringConfig): Spring;

  getSpringConfig(): SpringConfig;

  setCurrentValue(currentValue: number, skipSetAtRest?: boolean): Spring;

  getCurrentValue(): number;

  getStartValue(): number;

  getCurrentDisplacementDistance(): number;

  getDisplacementDistanceForState(state: PhysicsState): number;

  setEndValue(value: number): Spring;

  getEndValue(): number;

  setVelocity(value: number): Spring;

  getVelocity(): number;

  getRestSpeedThreshold(): number;

  setRestSpeedThreshold(value: number): Spring;

  setRestDisplacementThreshold(displacementFromRestThreshold: number): void;

  getRestDisplacementThreshold(): number;

  setOvershootClampingEnabled(value: boolean): Spring;

  isOvershootClampingEnabled(): boolean;

  isOvershooting(): boolean;

  advance(time: number, realDeltaTime: number): void;

  calculateNewPositionVelocity(tension: number, friction: number, tempPosition: number, tempVelocity: number, position: number, velocity: number): NewPosition;

  notifyPositionUpdated(notifyActivate?: boolean, notifyAtRest?: boolean): void;

  systemShouldAdvance(): boolean;

  wasAtRest(): boolean;

  isAtRest(): boolean;

  setAtRest(): Spring;

  _interpolate(alpha: number): void;

  getListeners(): Array<Listener>;

  addListener(value: Listener): Spring;

  removeListener(value: Listener): Spring;

  removeAllListeners(): Spring;

  currentValueIsApproximately(value: number): boolean;
}

export class SpringConfig {
  tension: number;
  friction: number;

  static DEFAULT_ORIGAMI_SPRING_CONFIG: SpringConfig;

  constructor(tension: number, friction: number);

  static fromOrigamiTensionAndFriction(tension: number, friction: number): SpringConfig;

  static fromBouncinessAndSpeed(bounciness: number, speed: number): SpringConfig;

  static coastingConfigWithOrigamiFriction(friction: number): SpringConfig;
}

export class AnimationLooper implements Looper {
  springSystem: SpringSystem;

  constructor();

  run(): void;
}

export class SimulationLooper implements Looper {
  springSystem: SpringSystem;

  constructor(timestep?: number);

  run(): void;
}

export class SteppingSimulationLooper extends SimulationLooper {
  step(timestep: number): void;
}

export namespace OrigamiValueConverter {
  function tensionFromOrigamiValue(value: number): number;

  function origamiValueFromTension(value: number): number;

  function frictionFromOrigamiValue(value: number): number;

  function origamiFromFriction(value: number): number;
}

export namespace MathUtil {
  function mapValueInRange(
    value: number, fromLow: number, fromHigh: number, toLow: number,
    toHigh: number): number;

  function interpolateColor(
    val: number, startColor: string, endColor: string, fromLow?: number,
    fromHigh?: number, asRGB?: boolean): string;

  function degreesToRadians(value: number): number;

  function radiansToDegrees(value: number): number;
}

export class BouncyConversion {
  bounciness: number;
  speed: number;

  constructor(bounciness: number, speed: number);

  normalize(value: number, startValue: number, endValue: number): void;

  projectNormal(n: number, start: number, end: number): void;

  linearInterpolation(t: number, start: number, end: number): void;

  quadraticOutInterpolation(t: number, start: number, end: number): void;

  b3Friction1(x: number): void;

  b3Friction2(x: number): void;

  b3Friction3(x: number): void;

  b3Nobounce(tension: number): void;
}

export const requestAnimationFrame;

export type RequestAnimationFrame = typeof requestAnimationFrame;

export namespace util {
  function extend<T, U>(target: T, source: U): T & U;

  function hexToRGB(color: string): RGB;

  function rgbToHex(r: number, g: number, b: number): string;

  const onFrame: RequestAnimationFrame;

  function removeFirst<T, U>(array: Array<T>, item: U): void;
}